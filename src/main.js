var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var numOfParticles = 64
var particles = []
var particleHistory = create2DArray(numOfParticles, 1, [], true)
var limit = 0.4

var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfParticles; i++) {
    particles.push(new BrownianParticle())
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < particles.length; i++) {
    particles[i].move()
    particles[i].display()
    particleHistory[i].push([particles[i].x, particles[i].y, particles[i].size])

    for (var j = 0; j < particleHistory[i].length - 1; j++) {
      stroke(255 * (j / particleHistory[i].length))
      strokeWeight(particleHistory[i][j][2] * boardSize)
      strokeCap(ROUND)
      noFill()
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      line(particleHistory[i][j + 1][0] * boardSize, particleHistory[i][j + 1][1] * boardSize, particleHistory[i][j][0] * boardSize, particleHistory[i][j][1] * boardSize)
      pop()
    }
    for (var j = 0; j < particleHistory[i].length; j++) {
      noStroke()
      fill(255 * (j / particleHistory[i].length))
      push()
      translate(windowWidth * 0.5, windowHeight * 0.5)
      ellipse(particleHistory[i][j][0] * boardSize, particleHistory[i][j][1] * boardSize, particleHistory[i][j][2] * boardSize)
      pop()
      if (particleHistory[i].length > 16) {
        particleHistory[i] = particleHistory[i].splice(1)
      }
    }
  }
}

class BrownianParticle {
  constructor() {
    var randAngle = Math.random() * Math.PI * 2
    var randDistance = -0.2 + 0.4 * Math.random()
    this.x = randDistance * sin(randAngle)
    this.y = randDistance * cos(randAngle)
    this.size = 0.01 + Math.random() * 0.04
  }

  move() {
    this.x += -0.02 + 0.04 * Math.random()
    this.y += -0.02 + 0.04 * Math.random()
    this.size += -0.002 + 0.004 * Math.random()
    if (this.size > 0.1) {
      this.size = 0.01
    }
    if (dist(this.x, this.y, 0, 0) > 0.4) {
      var randAngle = Math.random() * Math.PI * 2
      var randDistance = -0.2 + 0.4 * Math.random()
      this.x = randDistance * sin(randAngle)
      this.y = randDistance * cos(randAngle)
    }
  }

  display() {
    noStroke()
    fill(255)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    ellipse(this.x * boardSize, this.y * boardSize, this.size * boardSize)
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
